﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookDemo.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookDemo.Controllers
{
    [Produces("application/json")]
    [Route("api/Books")]
    public class BookController : Controller
    {
        MongoDBContext _context;
        public BookController()
        {
            _context = new MongoDBContext();
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable Get()
        {
            return _context.GetBooks();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Book Get(int id)
        {
            return _context.GetBook(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Book book)
        {
            _context.InsertBook(book);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put([FromBody]Book book)
        {
            _context.ReplaceBook(book);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _context.RemoveBook(id);
        }
    }
}
