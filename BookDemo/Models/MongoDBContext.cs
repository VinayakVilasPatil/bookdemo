﻿using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookDemo.Models
{
    public class MongoDBContext
    {
        private readonly IMongoDatabase _database = null;
        public MongoDBContext()
        { 
            var client = new MongoClient("mongodb://localhost:27017");
            if (client != null)
                _database = client.GetDatabase("BookDemo");
        }
        public IMongoCollection<Book> Books
        {
            get
            {
                return _database.GetCollection<Book>("Books");
            }
        }
        public IEnumerable GetBooks()
        {
            return this.Books.Find(b => true).ToList();
        }

        public Book GetBook(int id)
        {
            var filter = Builders<Book>.Filter.Eq("BookId", id);
            return this.Books.Find(filter).FirstOrDefault();
        }

        public Book InsertBook(Book book)
        {
            this.Books.InsertOne(book);
            return book;
        }
        public void ReplaceBook(Book book)
        {
            var filter = Builders<Book>.Filter.Eq("BookId", book.BookId);
            this.Books.ReplaceOne(filter, book);
        }
        public void RemoveBook(int id)
        {
            var filter = Builders<Book>.Filter.Eq("BookId", id);
            this.Books.DeleteOne(filter);
        }
    }
}
