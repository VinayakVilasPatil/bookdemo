﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace BookDemo.Models
{
    public class DataAccess
    {
        public static void SeedDatabase(MongoDBContext context)
        {
            if (context.Books.Find(b => true).Count() != 0) return;
            // TODO: Complete Implementation
            List<Book> e = new List<Book>()
           {
               new Book{BookId=1,title="C#",author="Bj",Price=124},
               new Book{BookId=2,title="Java",author="Vp",Price=150},
               new Book{BookId=3,title="Perl",author="Venky",Price=56},
               new Book{BookId=4,title="Rubby",author="Ravi",Price=32}
           };
            context.Books.InsertMany(e);
        }
    }
}
