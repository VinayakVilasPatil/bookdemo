﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookDemo.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string title { get; set; }
        public string author { get; set; }
        public double Price { get; set; }
    }
}
